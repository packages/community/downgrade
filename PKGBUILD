# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Stefano Capitani <stefano[at]manjaro[dot]org>
# Contributor: Helmut Stult
# Contributor: Patrick Brisbin <pbrisbin@gmail.com>

pkgname=downgrade
pkgver=11.4.4
pkgrel=1
pkgdesc="Bash script for downgrading one or more packages to a version in your cache or the A.L.A."
arch=('any')
url="https://github.com/archlinux-downgrade/downgrade"
license=('GPL-2.0-or-later')
depends=(
  'bash'
  'fzf'
  'pacman-contrib'
  'pacman-mirrors'
)
checkdepends=('python-cram')
backup=("etc/xdg/$pkgname/$pkgname.conf")
source=("$pkgname-$pkgver.tar.gz::$url/archive/v$pkgver.tar.gz"
        'close-ala-in-stable.patch')
sha256sums=('f369165cda0bf6113549c79a2517ad9ab77edf9a94ff66d5a022f357d4b7c517'
            '965747b1d89671274b8702289cb612f714941e84c92b2467c1d2b602803ce684')

prepare() {
  cd "$pkgname-$pkgver"
  patch -Np1 -i "$srcdir/close-ala-in-stable.patch"
  rm bin/downgrade.orig  # leftover from patching, Makefile installs it if not removed
}

check() {
  cd "$pkgname-$pkgver"
  cram test
}

package() {
  cd "$pkgname-$pkgver"
  make PREFIX=/usr DESTDIR="$pkgdir" install
}
